import { Component } from '@angular/core';
import { LoadingService } from './shares/services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'simpsons';

  constructor(public loadingService: LoadingService){

  }
}
