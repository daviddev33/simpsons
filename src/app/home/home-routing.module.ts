import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';

const routes: Routes = [
  {path:'home',component:HomeComponent,
     children:[
       {path:'',
           loadChildren: () => import ('../personajes/personajes.module').then(m => m.PersonajesModule) 
       }
     ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
