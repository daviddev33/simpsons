import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonajesRoutingModule } from './personajes-routing.module';
import { PersonajesListaComponent } from './components/personajes-lista/personajes-lista.component';
import { PersonajesFormularioComponent } from './components/personajes-formulario/personajes-formulario.component';
import { UtilsModule } from '../utils/utils.module';
import { MaterialDesingModule } from '../material-desing/material-desing.module';



@NgModule({
  declarations: [
    PersonajesListaComponent,
    PersonajesFormularioComponent
  ],
  imports: [
    CommonModule,
    PersonajesRoutingModule,
    UtilsModule,
    MaterialDesingModule
  ]
})
export class PersonajesModule { }
