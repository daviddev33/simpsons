import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonajesFormularioComponent } from './components/personajes-formulario/personajes-formulario.component';
import { PersonajesListaComponent } from './components/personajes-lista/personajes-lista.component';

const routes: Routes = [
  {path:'',component:PersonajesListaComponent},
  {path:'form',component:PersonajesFormularioComponent},
  {path:'form/:id',component:PersonajesFormularioComponent},  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonajesRoutingModule { }
