import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PersonajesService } from '../../services/personajes.service';
import {Personajes} from "../../Models/Personajes";
import {DialogImgComponent} from "../../../utils/dialog-img/dialog-img.component";
import { MatDialog } from '@angular/material/dialog';
import { LoadingService } from 'src/app/shares/services/loading.service';


@Component({
  selector: 'app-personajes-lista',
  templateUrl: './personajes-lista.component.html',
  styleUrls: ['./personajes-lista.component.scss']
})
export class PersonajesListaComponent implements OnInit {
  personajes:Personajes[]=[];
  

  constructor(private service:PersonajesService,
              private dialog: MatDialog,
              private loadingService:LoadingService) { }

  ngOnInit(): void {  
    this.getPersonajes();
  }
  getPersonajes(){   
      this.loadingService.setLoading(true);
      this.service.getPersonajes()
      .subscribe(
        res=>{
          this.personajes=res;
          console.log(this.personajes);
          this.loadingService.setLoading(false);        
        },
        err=>{
          console.log(err);
          this.loadingService.setLoading(false); 
        }
      )
  }
  openDialog(img:string){
    if(screen.width > 900){
      this.dialogCard(img);
    }   
  }
  dialogCard(img:string){
    this.dialog.open(DialogImgComponent, {
      data: { imagen: img },  
      panelClass: 'dialog-success'
    }); 
  }
  open(e:string,img:string){
    this.dialogCard(img);    
  }


}
