import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Personajes } from "../Models/Personajes";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonajesService {
  private api="https://thesimpsonsquoteapi.glitch.me/quotes?count=200";

  constructor(private http:HttpClient) { }

  getPersonajes():Observable<Personajes[]>{
    return this.http.get<Personajes[]>(this.api);     
  }
}
