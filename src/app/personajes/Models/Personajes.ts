

export interface Personajes {
    quote:              string;
    character:          string;
    image:              string;
    characterDirection?: CharacterDirection;
}

export enum CharacterDirection {
    Left = "Left",
    Right = "Right",
}
