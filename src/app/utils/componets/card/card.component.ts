import { Component, Input, OnInit, Output,EventEmitter} from '@angular/core';
import {Personajes} from "../../../personajes/Models/Personajes";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  ancho:number=screen.width;
  restore:boolean=false;
  position:string="";
  @Output() openDialog = new EventEmitter<string>();

  @Input() public dates: Personajes = {
    quote:              "",
    character:          "",
    image:             ""
  };

  constructor() { }

  ngOnInit(): void {
  }
  reestablecer(position:string){
    if(screen.width < 900){
      this.position=position;
      if(this.position == 'back'){
        this.restore = true;
      }else{
        this.restore = false;      
      }
    }
  }
  openCard(){
    this.openDialog.emit("true");
  }

}
