import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './componets/card/card.component';
import { DialogImgComponent } from './dialog-img/dialog-img.component';
import { MaterialDesingModule } from '../material-desing/material-desing.module';




@NgModule({
  declarations: [
    CardComponent,
    DialogImgComponent
  ],
  imports: [
    CommonModule,
    MaterialDesingModule 
  ],
  exports:[
    CardComponent,
    DialogImgComponent
  ],
  
})
export class UtilsModule { }
