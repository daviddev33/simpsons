import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-img',
  templateUrl: './dialog-img.component.html',
  styleUrls: ['./dialog-img.component.scss']
})
export class DialogImgComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {imagen: string} ) { }

  ngOnInit(): void {
    console.log(this.data.imagen);
  }

}
